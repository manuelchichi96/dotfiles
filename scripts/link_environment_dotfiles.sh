#!/usr/bin/env bash 

ENVIRONMENT_ARRAY="$@"

for environment in $ENVIRONMENT_ARRAY; do
  for filename in $(find environments/$environment -type f ! -name .git); do
    ln -sf $filename  $(basename $filename)
  done
done
