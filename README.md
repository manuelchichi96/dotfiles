# Manuel Chichi's Dotfiles

These dotfiles contain personalized configurations and scripts curated
specifically for DevOps tasks and workflows. Feel free to explore and adapt
anything you find useful!

## Usage

To generate the necessary symlinks, run the script and specify the environments
you'll utilize.

```sh
## If you want to add only desktop environment
./scripts/link_environment_dotfiles.sh desktop
## If you want to include more environments
./scripts/link_environment_dotfiles.sh desktop private
```
